package ru.vpavlova.tm.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.vpavlova.tm.configuration.ClientConfiguration;
import ru.vpavlova.tm.marker.IntegrationCategory;

public class UserEndpointTest {

    @NotNull
    public final AnnotationConfigApplicationContext context =
            new AnnotationConfigApplicationContext(ClientConfiguration.class);

    @NotNull
    private final SessionEndpoint sessionEndpoint = context.getBean(SessionEndpoint.class);

    @NotNull
    private final UserEndpoint userEndpoint = context.getBean(UserEndpoint.class);

    private final AdminEndpoint adminEndpoint = context.getBean(AdminEndpoint.class);

    @Nullable
    private Session session;

    @Nullable
    private Session sessionAdmin;

    @Before
    @SneakyThrows
    public void before() {
        session = sessionEndpoint.openSession("test", "test");
        sessionAdmin = sessionEndpoint.openSession("admin", "admin");
    }

    @After
    @SneakyThrows
    public void after() {
        sessionEndpoint.closeSession(session);
        sessionEndpoint.closeSession(sessionAdmin);
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void findUserByLoginTest() {
        adminEndpoint.createUser(session,"test", "test@twst.ru");
        Assert.assertNotNull(userEndpoint.findUserByLogin(session, "test"));
        adminEndpoint.removeOneByLogin(sessionAdmin, "test");
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void findUserOneBySessionTest() {
        final String login = userEndpoint.findUserOneBySession(session).getLogin();
        Assert.assertEquals("test", login);
    }

}
