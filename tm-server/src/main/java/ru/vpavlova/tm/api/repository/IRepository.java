package ru.vpavlova.tm.api.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import ru.vpavlova.tm.dto.AbstractEntity;

public interface IRepository<E extends AbstractEntity> extends JpaRepository<E, String> {

}
